package pizza

/**
 * Interfaz que representa un tamaño con un factor de precio en base al tamaño.
 */
interface Tamanio 
{
	def String nombre()

	def double factorDeTamanio()	
}
